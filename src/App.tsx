import React, {useEffect} from 'react';
import NavBar from "./components/NavBar";
import {Switch, Route, Redirect} from 'react-router-dom';
import HomePage from "./pages/HomePage";
import {eRoutes} from "./utils/routes";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import {useActions} from "./hooks/useActions";
import {useTypedSelector} from "./hooks/useTypedSelector";
import FilePage from "./pages/FilePage";
import Footer from "./components/commons/Footer";
import Loader from "./components/commons/Loader";

function App() {
  const {authUser} = useActions();
  const {isAuth, loading} = useTypedSelector(state => state.user)
  useEffect(() => {
    authUser();
  }, [])
  if(loading) return <Loader/>
  return (
  <div className={'app'}>
    <NavBar/>
    <div className={'wrapper'}>
        {!isAuth
            ?<Switch>
              <Route path={eRoutes.HOME_ROUTE} exact><HomePage/></Route>
              <Route path={eRoutes.REGISTER_ROUTE}><RegisterPage/></Route>
              <Route path={eRoutes.LOGIN_ROUTE}><LoginPage/></Route>
            </Switch>:
            <Switch>
              <Route exact path={eRoutes.HOME_ROUTE}><FilePage/></Route>
              <Redirect to={"/"}/>
            </Switch>
        }
    </div>
    <Footer/>

  </div>
  );
}

export default App;
