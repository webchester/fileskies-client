
export interface IFile {
    _id: string;
    name:string;
    type: string;
    size: string;
    accessLink: string | null;
    path: string;
    user: number;
    date: string;
    parent: number | null;
    child: number[];
}

interface IFileUpload{
    file: Blob;
}

export interface IFileState {
    files: IFile[];
    currentDir: IFile | null;
    errors: string | null;
    loading: boolean;
    popup: boolean;
    dirStack: IFile[];
    upload?: IFileUpload;
    switcher: string;
}

// Типы экшенов
export enum FileActionType{
    SET_FILES= "FILES.SET_FILES", // Подгрузка всех файлов
    SET_CURRENT_DIR = "FILES.SET_CURRENT_DIR",
    SET_FILE_ERRORS = "FILES.SET_FILE_ERRORS",
    SET_FILE_LOADING = "FILES.SET_FILE_LOADING",
    POPUP_TOGGLE = "FILES.POPUP_TOGGLE",
    CREATE_NEW_FILE = "FILES.CREATE_NEW_FILE",
    SET_DIR_STACK = "FILES.SET_DIR_STACK",
    UPLOAD_FILE = "FILES.UPLOAD_FILE",
    DELETE_FILE = "FILES.DELETE_FILE",
    SWITCH_VIEW = "FILES.SWITCH_FILE"
}

interface FileSetAction{
    type: FileActionType.SET_FILES
    payload: IFile[]
}
interface FileSetErrorAction{
    type: FileActionType.SET_FILE_ERRORS;
    payload: string;
}
interface FileLoadingAction{
    type: FileActionType.SET_FILE_LOADING;
}

interface FileCurrentDirAction{
    type: FileActionType.SET_CURRENT_DIR;
    payload: IFile | null
}

interface TogglePopup{
    type: FileActionType.POPUP_TOGGLE
}

interface CreateNewFile{
    type: FileActionType.CREATE_NEW_FILE;
    payload: IFile;
}

interface SetDirStackAction{
    type: FileActionType.SET_DIR_STACK,
    payload: IFile[]
}

interface UploadFileAction{
    type: FileActionType.UPLOAD_FILE;
    payload: IFile;
}

interface DeleteFileAction{
    type: FileActionType.DELETE_FILE;
    payload: string;
}

interface SwitchViewAction{
    type: FileActionType.SWITCH_VIEW;
    payload: string;
}


export type FileActions = 
      FileSetAction
    | FileCurrentDirAction 
    | FileLoadingAction 
    | FileSetErrorAction 
    | TogglePopup
    | CreateNewFile
    | SetDirStackAction
    | UploadFileAction
    | DeleteFileAction
    | SwitchViewAction;