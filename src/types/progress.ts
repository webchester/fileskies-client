export enum ProgressTypes {
    HIDE_BAR= 'PROGRESS.HIDDEN_BAR',
    SHOW_BAR = 'PROGRESS.SHOW_BAR',
    SET_PROGRESS = 'PROGRESS.SET_PROGRESS',
    SET_FILE = 'PROGRESS.SET_FILE'
}


export interface IProgressFile{
    id: string;
    name: string;
    progress: number;
}

export interface IProgressState {
    files: IProgressFile[],
    showBar: boolean
}

interface ProgressShowAction {
    type: ProgressTypes.SHOW_BAR;
}
interface ProgressHideAction {
    type: ProgressTypes.HIDE_BAR;
}

interface ProgressSetProgressAction{
    type: ProgressTypes.SET_PROGRESS;
    payload:IProgressFile
}

interface ProgressSetFileAction {
    type: ProgressTypes.SET_FILE;
    payload: IProgressFile
}

export type ProgressActions = ProgressHideAction | ProgressSetProgressAction | ProgressShowAction | ProgressSetFileAction