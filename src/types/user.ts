import {IFile} from './files';

export interface IUser {
    email: string;
    diskSpace: number;
    usedSpace: number;
    avatar: string;
    files: IFile[];
}

export interface IUserState {
    token: string | null;
    user: IUser | null;
    isAuth: boolean;
    error: string | null;
    loading: boolean;
}

export interface IUserAuth{
    email: string;
    password: string;
}

export enum UserActionTypes{
    FETCH_USER = "FETCH_USER",
    FETCH_LOGIN = 'FETCH_LOGIN',
    FETCH_REGISTRATION = 'FETCH_REGISTRATION',
    FETCH_ERROR = 'FETCH_USER_ERROR',
    LOGOUT_USER = 'LOGOUT_USER',
    AUTH_USER = 'AUTH_USER'
}

interface FetchUserErrorAction {
    type: UserActionTypes.FETCH_ERROR;
    payload: string;
}

interface FetchUserLoginAction {
    type: UserActionTypes.FETCH_LOGIN;
    payload: IUser;
}

interface FetchUserRegistrationAction {
    type: UserActionTypes.FETCH_REGISTRATION;
    payload: IUser;
}

interface FetchUser {
    type: UserActionTypes.FETCH_USER;
}
interface  LogoutUser {
    type: UserActionTypes.LOGOUT_USER;
}
interface UserAuth {
    type: UserActionTypes.AUTH_USER;
    payload: IUser;
}

export type UserActions = FetchUserLoginAction
    | FetchUserRegistrationAction
    | UserAuth
    | FetchUserErrorAction
    | FetchUser
    | LogoutUser;