import {eRequests, errorResponse, inst, instWithAuth} from "./settings";
import {IUser} from "../types/user";

export  const userRequest = {
    // auth user
    fetchUser:async ():Promise<IUser>  => {
        try{
            const {data} = await instWithAuth.get(eRequests.USER_ME);
            return data.me;
        }catch (error: any){
            throw errorResponse(error);
        }
    },
    fetchLogin: async (email: string, password: string)=>{
        try{
            const {data} = await inst.post(eRequests.USER_LOGIN, {email, password});
            return data;
        }catch (error: any){
            throw errorResponse(error);
        }
    },
    fetchRegister: async (email: string, password: string) =>{
        try{
            const {data} = await inst.post(eRequests.USER_REGISTRATION, {email, password});
            return data;
        }catch (error: any){
            throw errorResponse(error);
        }
    }
}
