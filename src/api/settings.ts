import axios, {AxiosError, AxiosInstance} from "axios";

export enum eRequests {
    FILE = 'file',
    FILE_UPLOAD ='file/upload',
    FILE_DOWNLOAD ='file/download',

    USER_ME = 'user/me',
    USER_LOGIN = 'user/login',
    USER_REGISTRATION = 'user/registration',
    SEARCH = 'file/search'

}

export interface ServerError {
    message: string
}
// without auth needed
const inst: AxiosInstance = axios.create({
        baseURL: process.env.REACT_APP_SERVER_URL
    });


// with auth
const instWithAuth: AxiosInstance = axios.create({
    baseURL: process.env.REACT_APP_SERVER_URL
});

instWithAuth.interceptors.request.use( function(config){
    const token = localStorage.getItem('token');
    config.headers.Authorization = `Bearer ${token}`;
    return config;
});

const errorResponse = (error: AxiosError<any>):string=> {
    if (error.response) {
        console.log(error.response.data.message);
        return error.response.data.message;
    }else{
        return error.message;
    }
}

export {
    inst, instWithAuth, errorResponse
}

