import {eRequests, errorResponse, instWithAuth} from './settings';
import {IFile} from "../types/files";
import {ProgressActions, ProgressTypes} from "../types/progress";
import {Dispatch} from "redux";

interface IFindParam {
    parent?: string;
    sort?:string;
}

export const  fileRequest = {
    searchFiles: async (search: string):Promise<IFile[]> => {
        try{
            const {data} = await instWithAuth.get(eRequests.SEARCH, {params: {search}});
            return data;
        }catch (error) {
            throw errorResponse(error);
        }
    },
    getFiles: async (parent: string | null = '', sort: string): Promise<IFile[]> => {
        try {
            let findParent: IFindParam = {sort:sort};
            if(parent) findParent['parent'] = parent;
            const {data} = await instWithAuth.get(eRequests.FILE, {params: findParent});
            return data;
        } catch (error) {
            throw errorResponse(error);
        }
    },
    createFile: async (name:string, parent: string | null, type: string ): Promise<IFile> => {
        try{
            // name, 
            const {data} = await instWithAuth.post(eRequests.FILE, {name, parent, type});
            return data;
        }catch(error){
            throw errorResponse(error);
        }
    },
    uploadFile: async (file: any,
                       dirId: string |null,
                       dispatch:Dispatch<ProgressActions>,
                       id: string): Promise<IFile> => {
        try{
            const formData = new FormData();
            formData.append('file', file);
           if(dirId) formData.append('parent', dirId);
            const {data} = await instWithAuth.post(eRequests.FILE_UPLOAD, formData, {onUploadProgress: (progressEvent => {
                const totalLength = progressEvent.lengthComputable
                    ? progressEvent.total
                    : progressEvent.target.getResponseHeader('content-length')
                        || progressEvent.target.getResponseHeader('x-decompressed-content-length');
                    if(totalLength){
                        let progress = Math.round(progressEvent.loaded * 100) / totalLength;
                        dispatch({ type: ProgressTypes.SET_PROGRESS,
                            payload: {id:id,name:String(file.name), progress: progress}})
                    }
            })});
            return data;
        }catch (error){
           throw errorResponse(error);
        }
    },

    downloadFile: async (file:IFile) =>{
        const response = await fetch(`${process.env.REACT_APP_SERVER_URL}${eRequests.FILE_DOWNLOAD}?id=${file._id}`,{
            headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
        });
        if(response.status === 200) {
            const blob = await response.blob();
            const downloadUrl = window.URL.createObjectURL(blob);
            const link = document.createElement('a');
            link.href = downloadUrl;
            link.download = file.name;
            document.body.appendChild(link);
            link.click();
            link.remove();
        }
    },
    deleteFile: async (id:string): Promise<any> => {
            try{
                const {data} = await instWithAuth.delete(`${eRequests.FILE}?id=${id}`);
                return data;
            }catch (error){
                throw errorResponse(error);
            }
    }
}