import {IProgressState, ProgressActions, ProgressTypes} from "../../types/progress";

const initialState: IProgressState = {
    files: [],
    showBar: false
}

export const progressReducer  = (state: IProgressState = initialState, action:ProgressActions): IProgressState => {

    switch (action.type){
        case ProgressTypes.SET_FILE:
            return {...state, files: [...state.files, action.payload]}
        case ProgressTypes.SHOW_BAR:
            return {...state, showBar:true}
        case ProgressTypes.HIDE_BAR:
            return {...state, showBar: false}
        case ProgressTypes.SET_PROGRESS:
            return { ...state,
                files: [...state.files.map((f) =>
                    f.id === action.payload.id
                    ? {...f,  progress: action.payload.progress } : {...f})]
            }
        default:
            return {...state}
    }
}