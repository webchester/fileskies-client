import {IUserState, UserActions, UserActionTypes} from "../../types/user";

const initialState: IUserState = {
    token: null,
    user: null,
    error: null,
    isAuth: false,
    loading: false
}
export const userReducer = (state = initialState, action: UserActions): IUserState  => {

    switch (action.type){
        case UserActionTypes.FETCH_USER:
            return {...state, loading: true}
        case UserActionTypes.FETCH_LOGIN:
            return {...state, user: action.payload, loading: false, isAuth:true };
        case UserActionTypes.FETCH_REGISTRATION:
            return {...state, user: null, loading: false, isAuth: false };
        case UserActionTypes.FETCH_ERROR:
            return {...state, loading: false, user: null, isAuth: false, error: action.payload }
        case UserActionTypes.LOGOUT_USER:
            return {...state, user: null, isAuth: false}
        case UserActionTypes.AUTH_USER:
            return {...state, user: action.payload, loading:false, isAuth: true}
        default:
            return state;
    }
}