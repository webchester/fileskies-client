import {combineReducers} from "redux";
import {userReducer} from "./userReducer";
import {fileReducer} from "./fileReducer";
import {progressReducer} from "./progressReducer";

export const rootReducer = combineReducers({
    user: userReducer,
    file: fileReducer,
    progress: progressReducer
});

export type RootState = ReturnType<typeof rootReducer>