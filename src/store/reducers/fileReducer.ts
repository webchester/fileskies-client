import {FileActions, FileActionType, IFileState} from "../../types/files";

const initialState: IFileState = {
    files: [],
    currentDir: null,
    loading: false,
    errors: null,
    popup: false,
    dirStack: [],
    upload: undefined,
    switcher: 'list'
}

export const fileReducer = (state = initialState, action: FileActions ): IFileState =>{
    switch (action.type){
        case FileActionType.SET_CURRENT_DIR:
            return {...state, currentDir: action.payload, loading:false}
        case FileActionType.SET_FILES:
            return {...state, files: action.payload, loading:false}
        case FileActionType.SET_FILE_ERRORS:
            return {...state, errors: action.payload, loading:false}
        case FileActionType.SET_FILE_LOADING:
            return {...state, loading: true};
        case FileActionType.POPUP_TOGGLE:
            return {...state, popup: !state.popup}
        case FileActionType.CREATE_NEW_FILE:
            return {...state, loading: false, files: [action.payload, ...state.files ] }
        case FileActionType.SET_DIR_STACK:
            return {...state, loading: false, dirStack: action.payload}
        case FileActionType.DELETE_FILE:
            return {...state, files: [...state.files.filter(f => f._id !== action.payload)]}
        case FileActionType.SWITCH_VIEW:
            return {...state, switcher: action.payload}
        default: return state;
    }
}