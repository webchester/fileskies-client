import {UserActions, UserActionTypes} from "../../types/user";
import {Dispatch} from "redux";
import {userRequest} from "../../api/userRequest";
import {withServer} from "../../hooks/withServer";


export const removeError = () => (dispatch: Dispatch<UserActions>): void => {
    dispatch({type: UserActionTypes.FETCH_ERROR, payload: ''});
}

export const authUser = () =>async (dispatch: Dispatch<UserActions>) => {
    if(localStorage.getItem('token')){
        try{
            dispatch({type: UserActionTypes.FETCH_USER});
            const user = await userRequest.fetchUser();
            dispatch({type: UserActionTypes.AUTH_USER, payload: {...user, avatar: withServer(user.avatar)}})
        }catch (error){
            if(typeof  error === 'object' && error.errors) {
                dispatch({type: UserActionTypes.FETCH_ERROR, payload: String(error.errors[0].msg)});
            }else{
                dispatch({type: UserActionTypes.FETCH_ERROR, payload: String(error)});
            }

        }
    }
}


export const logoutUser = () => (dispatch: Dispatch<UserActions>) =>{
    localStorage.removeItem('token');
    dispatch({type: UserActionTypes.LOGOUT_USER})
    dispatch({type: UserActionTypes.FETCH_ERROR, payload: ''});
}

export const fetchRegistrationUser = (email: string, password: string) => async (dispatch: Dispatch<UserActions>) => {

    try{
        const userResponse = await userRequest.fetchRegister(email, password);
        localStorage.setItem('token', userResponse.token);
        dispatch({type: UserActionTypes.FETCH_LOGIN, payload: {...userResponse.user, avatar: withServer(userResponse.user.avatar)}});
    }catch (error){
        if(typeof  error === 'object' && error.errors) {
            dispatch({type: UserActionTypes.FETCH_ERROR, payload: String(error.errors[0].msg)});
        }else{
            dispatch({type: UserActionTypes.FETCH_ERROR, payload: String(error)});
        }
    }
}

export const fetchLoginUser = (email:string, password:string) =>async (dispatch: Dispatch<UserActions>) =>{
    try{

        const login = await userRequest.fetchLogin(email, password);
        if(login){
            localStorage.setItem('token', login.token);
            dispatch({type: UserActionTypes.FETCH_LOGIN, payload: {...login.user, avatar: withServer(login.user.avatar)}});
        } else dispatch({type: UserActionTypes.FETCH_ERROR, payload: 'Не удалось авторизоваться'})
    }catch (error){
        if(typeof  error === 'object' && error.errors) {
            dispatch({type: UserActionTypes.FETCH_ERROR, payload: String(error.errors[0].msg)});
        }else{
            dispatch({type: UserActionTypes.FETCH_ERROR, payload: String(error)});
        }
    }
}

