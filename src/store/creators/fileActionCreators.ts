import {FileActions, FileActionType, IFile} from "../../types/files";
import {Dispatch} from "redux";
import {fileRequest} from "../../api/fileRequest";
import {ProgressActions, ProgressTypes} from "../../types/progress";


export const setSwitcher = (typeOfSwitch: string) => async (dispatch: Dispatch<FileActions>) => {
    await dispatch({type: FileActionType.SWITCH_VIEW, payload: typeOfSwitch});
}

export const searchFiles = (search: string) => async (dispatch: Dispatch<FileActions>):Promise<void> => {
    try{
    const files = await fileRequest.searchFiles(search);
    await dispatch({type: FileActionType.SET_FILES, payload: files})
    }catch (error){
        dispatch({type: FileActionType.SET_FILE_ERRORS, payload: String(error)});
    }
}

export const setFiles = (parent: string, sort: string) => async (dispatch: Dispatch<FileActions>) => {
    try{
        await dispatch({type: FileActionType.SET_FILE_LOADING});
        const files = await fileRequest.getFiles(parent, sort);
        await dispatch({type: FileActionType.SET_FILES, payload: files});
    } catch (error){
        await dispatch({type: FileActionType.SET_FILE_ERRORS, payload: String(error)})
    }
}

export const setCurrentDir  = (dir: IFile | null) => async (dispatch: Dispatch<FileActions>) => {
     dispatch({type: FileActionType.SET_CURRENT_DIR, payload: dir})
}

export const togglePopup =()=>async (dispatch: Dispatch<FileActions>) => {
    await dispatch({type: FileActionType.POPUP_TOGGLE});
}

export const createFolder = (name:string, type:string, parent: string | null) => async (dispatch: Dispatch<FileActions>) => {
    try{
        const createdFile = await fileRequest.createFile(name, parent, type);
        await dispatch({type: FileActionType.CREATE_NEW_FILE, payload: createdFile});
    }catch (error){
        await dispatch({type: FileActionType.SET_FILE_ERRORS, payload: String(error)})
    }
}

export const setDirStack = (dir: IFile[]) => async (dispatch: Dispatch<FileActions>) =>{
    //dispatch({type: FileActionType.SET_FILE_LOADING})
    dispatch({type: FileActionType.SET_DIR_STACK, payload: dir})
}

export const uploadFile = (file: any, dirId: string | null) => async (
    dispatch: Dispatch<FileActions | ProgressActions>) =>{
    try{
        const id= String(Date.now())
        dispatch({type: ProgressTypes.SHOW_BAR});
        dispatch({type: ProgressTypes.SET_FILE, payload: {id, name: file.name, progress: 0}});
        const fileLoaded = await fileRequest.uploadFile(file, dirId, dispatch, id);
        dispatch({type: FileActionType.UPLOAD_FILE, payload: fileLoaded});
    }catch (err){
        await dispatch({type: FileActionType.SET_FILE_ERRORS, payload: String(err)})
    }
}

export const deleteFile = (id: string) => async (dispatch: Dispatch<FileActions>): Promise<void> => {
    try{
        await fileRequest.deleteFile(id);
        await dispatch({ type: FileActionType.DELETE_FILE, payload: id });
    }catch (error){
        await dispatch({type: FileActionType.SET_FILE_ERRORS, payload: String(error)});
    }
}

export const deleteError = () => async (dispatch: Dispatch<FileActions>) => {
    dispatch({type: FileActionType.SET_FILE_ERRORS, payload: ''});
}