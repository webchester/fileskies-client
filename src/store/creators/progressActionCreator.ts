import {Dispatch} from "redux";
import {IProgressFile, ProgressActions, ProgressTypes} from "../../types/progress";


export const progressShow =() => (dispatch: Dispatch<ProgressActions>) => {
    dispatch({
        type: ProgressTypes.SHOW_BAR
    })
}
export const progressHide = () => (dispatch: Dispatch<ProgressActions>):void => {
    dispatch({
        type: ProgressTypes.HIDE_BAR
    })
}

export const progressSetFile = (file: IProgressFile) => (dispatch: Dispatch<ProgressActions>): void => {
    dispatch({
        type: ProgressTypes.SET_FILE,
        payload: file
    })
}

export const progressChange = (file: IProgressFile) => (dispatch: Dispatch<ProgressActions>): void =>{
    dispatch({
        type: ProgressTypes.SET_PROGRESS,
        payload: file
    })
}