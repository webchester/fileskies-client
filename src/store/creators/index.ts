import * as userActions from './userActionCreator';
import * as fileActions from './fileActionCreators';
import * as progressActions from './progressActionCreator';

export default {
    ...userActions, ... fileActions, ...progressActions
}