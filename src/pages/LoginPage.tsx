import React, {useState} from 'react';
import AuthForm from "../components/AuthForm";
import Input from "../components/Input";
import {NavLink, useHistory} from "react-router-dom";
import {eRoutes} from "../utils/routes";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useActions} from "../hooks/useActions";

const LoginPage = () => {
    const history = useHistory()
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const {isAuth} = useTypedSelector(state => state.user)
    const {fetchLoginUser, removeError}  = useActions();

    const submitHandler = async (e: React.MouseEvent<HTMLButtonElement>):Promise<void> =>{
        e.preventDefault();
        await setLoading(true);
        await removeError();
        await fetchLoginUser(email, password);
        await setLoading(false);
    }
    if(isAuth) history.push(eRoutes.HOME_ROUTE);
    return (
        <div className={'wrapper__center'}>
            <AuthForm title={'Вход'}>
                <Input type={'text'} value={email} label={'email'} placeholder={'Название'} changeHandler={(e)=>setEmail(e.target.value)}/>
                <Input type={'password'} value={password} label={'Пароль'} placeholder={'Название'} changeHandler={(e) => setPassword(e.target.value)}/>

                <div className={'btn-block auth-form__footer'}>
                    <div className={'btn-block--left'}>
                        Нет аккаунта?  <NavLink to={eRoutes.REGISTER_ROUTE}> Регистрация </NavLink>
                    </div>
                    <div className={'btn-block--right'}>
                        <button onClick={submitHandler} className={'btn btn--full'} disabled={loading} > {!loading ? 'Вход' : 'Авторизация ... '}</button>
                    </div>
                </div>

            </AuthForm>
        </div>
    );
};

export default LoginPage;