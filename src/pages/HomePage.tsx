import React from 'react';
import {NavLink} from 'react-router-dom';
import reactSvg from "../assets/icons/React-icon.svg";
import reduxSvg from "../assets/icons/redux.svg";
import nodeSvg from "../assets/icons/nodejs.svg";
import saasSvg from "../assets/icons/sass.svg";
import typeSvg from "../assets/icons/typescript.svg";
import mongoSvg from "../assets/icons/mongodb.svg";
import reduxSagaSvg from "../assets/icons/redux-saga.svg";
import jvtSvg from "../assets/icons/jwt-3.svg";
import '../styles/home.scss';
import {eRoutes} from "../utils/routes";

const HomePage= () => {
    return (
        <div className={'home'}>
            <div className={'home__section'}>
                <div className={'home-header'}>
                    <div className={'home-header__wrapper'}>
                        <div className={'home-header__text'}>
                            <div className={'home-header__titles'}>
                                <h1 className={'home-title'}>Онлайн хранилище и хранилище данных</h1>
                                <h2 className={'home-subtitle'}>Храните любые данные в защищенном хранилище в сети</h2>
                            </div>
                            <div className={'home-header__info'}>
                                <NavLink className={'btn header-btn'} to={eRoutes.REGISTER_ROUTE} >Регистрация</NavLink>
                            </div>

                        </div>
                        <div className={'home-header__media'}>
                            <img className={"home-header__icon"} src={`${process.env.PUBLIC_URL}/cloud-storage.png`} alt={'home header icon'}/>
                        </div>
                    </div>

                </div>
            </div>

            <div className={'home__section'}>
                <h1 className={'title'}>Технологии</h1>
                <div className={"tech"}>
                    <img className="tech__logo-icon" src={reactSvg} alt={'react'}/>
                    <img className="tech__logo-icon" src={reduxSvg} alt={'redux'}/>
                    <img className="tech__logo-icon" src={nodeSvg} alt={'nodejs'}/>
                    <img className="tech__logo-icon" src={saasSvg} alt={'saas'}/>
                    <img className="tech__logo-icon" src={typeSvg} alt={'type Script'}/>
                    <img className="tech__logo-icon" src={mongoSvg} alt={'mongo db'}/>
                    <img className="tech__logo-icon" src={reduxSagaSvg} alt={'redux saga'}/>
                    <img className="tech__logo-icon" src={jvtSvg} alt={'jwt'}/>

                </div>
            </div>

            <div className={'home__section'}>
                <div className={'home-header'}>
                    <div className={'home-header__wrapper'}>
                        <div className={'home-header__media'}>
                            <img className={"home-header__icon"} src={`${process.env.PUBLIC_URL}/space-files.png`} alt={'home header icon'}/>
                        </div>
                        <div className={'home-header__text'}>
                            <div className={'home-header__titles'}>
                                <h1 className={'home-title'}>Резервное копирование любого файла или папки</h1>
                                <h2 className={'home-subtitle'}>Независимо типа файла или папки, для которых требуется резервное копирование,
                                    — от фотографий и видеофайлов до больших файлов CAD и презентаций PowerPoint,
                                    — вы можете безопасно хранить их, используя решения облачного хранения</h2>
                            </div>
                            <div className={'home-header__info'}>
                                <NavLink className={'btn header-btn'} to={eRoutes.REGISTER_ROUTE} >Регистрация</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>
    );
};

export default HomePage;