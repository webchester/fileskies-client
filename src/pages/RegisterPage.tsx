import React, {useState} from 'react';
import AuthForm from "../components/AuthForm";
import Input from "../components/Input";
import {eRoutes} from "../utils/routes";
import {NavLink, useHistory} from 'react-router-dom';
import {useActions} from "../hooks/useActions";
import {useTypedSelector} from "../hooks/useTypedSelector";

const RegisterPage = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const {fetchRegistrationUser, removeError}  = useActions();
    const {isAuth} = useTypedSelector(state => state.user);
    const [loading, setLoading] = useState<boolean>(false);
    const history = useHistory();

    const submitHandler = async (e: React.MouseEvent<HTMLButtonElement>):Promise<void> => {
        e.preventDefault();
        await setLoading(true);
        await removeError();
        await fetchRegistrationUser(email, password);
        await setLoading(false);
    }
    if(isAuth) history.push(eRoutes.HOME_ROUTE);
    return (
        <div className={'wrapper__center'}>
            <AuthForm title={'Регистрация'}>
                <Input type={'text'}
                       value={email}
                       label={'email'}
                       placeholder={'Название'}
                       changeHandler={(e)=>setEmail(e.target.value)}/>
                <Input type={'password'}
                       value={password}
                       label={'Пароль'}
                       placeholder={'Название'}
                       changeHandler={e => setPassword(e.target.value)}/>
                <div className={'btn-block auth-form__footer'}>
                    <div className={'btn-block--left'}>
                        Есть аккаунт?  <NavLink to={eRoutes.LOGIN_ROUTE}> Авторизуйтесь</NavLink>
                    </div>
                    <div className={'btn-block--right'}>
                        <button  className={'btn btn--full'} onClick={submitHandler} disabled={loading} > {!loading ? 'Регистрация' : 'Регистрирую ... '}</button>
                    </div>
                </div>

            </AuthForm>
        </div>
    );
};

export default RegisterPage;