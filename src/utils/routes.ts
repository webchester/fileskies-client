
export enum eRoutes {
    HOME_ROUTE='/',
    REGISTER_ROUTE = '/register',
    LOGIN_ROUTE = '/login',
    FILES_ROUTE = '/files'
}
