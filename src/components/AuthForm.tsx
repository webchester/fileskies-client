import React from 'react';
import Logo from "./Logo";

import { useTypedSelector } from '../hooks/useTypedSelector';
import UserErrors from './UserErrors';
import '../styles/authform.scss';

interface AuthFormProps{
    title: string;
}

const AuthForm: React.FC<AuthFormProps> = ({title, children}) => {
    const error = useTypedSelector(state => state.user.error);

    return (
            <div className={'auth-card'}>
                <div className={'auth-card__inner'}>
                    <div className={'auth-card__header'}>
                        <Logo/>
                        <h1 className={'title'}>{title}</h1>
                    </div>
                    <form className={'auth-card__form'}>
                        {children}
                    </form>
                </div>
                {error  &&  <UserErrors error={error}/> }
            </div>
    );
};

export default AuthForm;