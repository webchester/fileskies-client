import React, {useEffect, useState} from 'react';
import {useActions} from "../../hooks/useActions";
import {useTypedSelector} from "../../hooks/useTypedSelector";
import FileList from "./FileList";
import '../../styles/disk.scss';
import Loader from "../commons/Loader";
import Popup from "./Popup";
import {IFile, IFileState} from "../../types/files";
import FileUpload from "./FileUpload";
import uploadSvg from '../../assets/icons/upload.svg';
import FileUploader from "./uploader/FileUploader";
import BxSortUpIcon from "../icons/BxSortUpIcon";
import Empty from "../commons/Empty";
import BxArrowBackIcon from "../icons/BxArrowBackIcon";
import BxMenuIcon from "../icons/BxMenuIcon";
import BxsGridAltIcon from "../icons/BxsGridAltIcon";
import BxsFolderPlusIcon from "../icons/BxsFolderPlusIcon";
import BxsFileImageIcon from "../icons/BxsFileImageIcon";


const Disk: React.FC = () => {
    const {setFiles, setSwitcher, togglePopup, setCurrentDir, setDirStack, uploadFile, deleteFile} = useActions();
    const {files,
        loading,
        currentDir,
        dirStack,
        switcher} = useTypedSelector<IFileState>(state => state.file);
    const [dragInter, setDragInter] = useState<boolean>(false);
    const [sort, setSort] = useState<string>('type');

    useEffect( () => {
        setFiles(currentDir?._id || '', sort);
    }, [currentDir, sort] );

    const onChangeOptionHandler = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setSort(e.target.value);
    }

    const changeCurrentDir = async (dir: IFile) => {
        if(dir.type==='dir') {
          if(currentDir)  await setDirStack([...dirStack, currentDir]);
            await setCurrentDir(dir);
        }
    }

    const backNavigationHandler = async () => {
        const lastDir: IFile | undefined = await dirStack.pop();
        if(typeof lastDir === 'object')
            setCurrentDir(lastDir);
        else
            await setCurrentDir(null)
        await setDirStack(dirStack);
    }

    const togglePopupHandler = () => {
        togglePopup();
    }

    const onChangeHandler = async (file: any[] | any) => {
        // 'for' because type fileList of file
        for (let f = 0; f<file.length; f++) {
            await uploadFile(file[f], currentDir?._id || '');
        }
        await setFiles(currentDir?._id || '', sort);
    }

    const onDragOpenHandler = (e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        e.stopPropagation();
        setDragInter(true);
    }

    const onDragClosedHandler = (e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        e.stopPropagation();

    }

    const onDrop = async (e: React.DragEvent<HTMLDivElement>):Promise<void> => {
        e.preventDefault();
        e.stopPropagation();
        setDragInter(false);
        await onChangeHandler(e.dataTransfer.files)
    }

    const onDeleteFileHandler = async (e: React.MouseEvent<HTMLButtonElement>, id: string): Promise<void> => {
        e.preventDefault();
        e.stopPropagation();
        await deleteFile(id);
    }

    if(loading) return <Loader/>

    return (<>
        {
            dragInter ?
        <div className={'drag-wrapper'}
             onDragEnter={(e: React.DragEvent<HTMLDivElement>) => onDragOpenHandler(e)}
             onDragLeave={(e: React.DragEvent<HTMLDivElement>) => onDragClosedHandler(e)}
             onDragOver={(e: React.DragEvent<HTMLDivElement>) => onDragOpenHandler(e)}
             onDrop={(e:React.DragEvent<HTMLDivElement>)=>onDrop(e)}
        >
            <div className={'drag-wrapper__container'} >
                <img className={'drag-wrapper__icon'} src={uploadSvg} alt={"upload icon"}/>
                Перетащите файлы
            </div>
        </div>
           :
        <div className={'disk'}>
            <FileUploader/>
                <Popup/>

                <div className={'disk__wrapper'}>
                <div className={'disk__panel'}>
                    <div className={'disk__panel-left'}>
                        {currentDir!==null && <button
                            className={'btn btn--outlined disk__panel-items'}
                            onClick={backNavigationHandler}>
                            <BxArrowBackIcon/>{dragInter}
                        </button> }
                        <button
                            title={'Добавить папку'}
                            className={'btn  btn--outlined disk__panel-items'}
                            onClick={togglePopupHandler}>
                           <BxsFolderPlusIcon/>
                        </button>
                        <FileUpload
                            label={<BxsFileImageIcon/>}
                            onChangeHandler={(file: any) => onChangeHandler(file)}
                        />
                    </div>
                    <div className={'disk__panel-middle'}>

                    </div>
                    <div className={'disk__panel-right'}>
                        <BxSortUpIcon/>
                        <select value={sort} className={'disk__select'} onChange={(e)=>onChangeOptionHandler(e)}>
                            <option value={'type'}>По типу</option>
                            <option value={'date'}>По дате</option>
                            <option value={'name'}>По имени</option>
                        </select>
                        <div className={"disk__panel-views"}>
                           <button
                               onClick={() => setSwitcher('list')}
                               className={`disk__panel-view-btn ${switcher==='list' ? ' disk__panel-view-btn--active' :''}`}>
                               <BxMenuIcon/>
                           </button>
                            <button
                                onClick={() => setSwitcher('grid')}
                                className={`disk__panel-view-btn ${switcher === 'grid' ? ' disk__panel-view-btn--active' : ''}`}><BxsGridAltIcon/></button>
                        </div>
                    </div>
                </div>

                <div className={'disk__content'}
                     onDragEnter={(e: React.DragEvent<HTMLDivElement>) => onDragOpenHandler(e)}
                     onDragLeave={(e: React.DragEvent<HTMLDivElement>) => onDragClosedHandler(e)}
                     onDragOver={(e: React.DragEvent<HTMLDivElement>) => onDragOpenHandler(e)}
                >

                    <ul className={'disk__navigation'}>

                        <li className={'disk__navigation-item disk__navigation-item--first-child'}>
                            Home
                        </li>
                        {dirStack.map(d => <li key={d._id} className={'disk__navigation-item'}>
                            {d.name}
                            </li>
                        )}
                        <li className={'disk__navigation-item '}> {currentDir?.name}</li>
                    </ul>
                    <h1 className={'disk__folder-name'}>{currentDir?.name}</h1>
                    {files.length<1 ?
                        <Empty/>
                    :
                        <FileList
                            switcher = {switcher}
                            files={files}
                            changeCurrentDir={changeCurrentDir}
                            onDeleteFileHandler = {onDeleteFileHandler}
                        />
                    }

                </div>
                </div>

        </div>}
        </>
    );
};

export default Disk;