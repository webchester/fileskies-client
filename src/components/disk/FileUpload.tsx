import React from 'react';

interface FileUploadProps{
    label: string | React.SVGProps<SVGSVGElement>;
    onChangeHandler: (file: any)=>void;
}

const FileUpload:React.FC<FileUploadProps> = ({label, onChangeHandler}) => {
    const ref = React.useRef<HTMLInputElement>(null);

    const changeHandler = (e :React.ChangeEvent<HTMLInputElement>) =>{
       if(e.currentTarget.files!==null) onChangeHandler(e.currentTarget.files);
     //   onChangeHandler(e.target.files![0]);
    }

    return (
        <div onClick={()=>ref.current!.click()}>
            <button className={'btn  disk__panel-items btn--outlined'} title={'Добавить фаил'}>{label}</button>
            <input
                ref={ref}
                multiple
                onChange={(event) => changeHandler(event)}
                style={{display:'none'}}
                type={'file'}  />
        </div>
    );
};

export default FileUpload;