import React, {useState} from 'react';
import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import '../../styles/popup.scss';
import CloseCircle from '../icons/CloseCircle';
import Input from '../Input';

const Popup = () => {
    const {popup, currentDir} = useTypedSelector(state => state.file);
    const {togglePopup, createFolder} = useActions();
    const [newFile, setNewFile] = useState<string>('');

    const toggleHandler = () => {
       togglePopup();
    }

    const createNewFile = ():void => {
        createFolder(newFile, "dir", currentDir?._id || null);
        setNewFile('');
        togglePopup();
    }
    

    return (
        <div className={`popup ${popup && 'popup--show'}`} >
            <div className={'popup__box'}>
                <div className={'popup__header'}>
                    <div className={'popup__title'}>Добавить папку</div>
                    <div className={'popup__close'} onClick={toggleHandler}><CloseCircle/></div>
                </div>
                <div className={'popup__wrapper'}>
                    <Input type={'text'} 
                           placeholder="Название папки" 
                           value={newFile}
                           changeHandler={(e) => setNewFile(e.target.value)} />
                </div>
                <div className={'popup__footer'}>
                    <div className={'popup__footer-left'}>

                    </div>
                    <div className={'popup__footer-right'}>
                        <button 
                            className={'btn popup--center'} 
                            onClick={createNewFile}>
                                Добавить папку
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Popup;