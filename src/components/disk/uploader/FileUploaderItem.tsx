import React from 'react';

interface FileUploaderItemProps{
    name: string;
    progress: number
}

const FileUploaderItem: React.FC<FileUploaderItemProps> = ({name, progress}) => {
    return (
        <div className={'upload-item'}>
            <div className={'upload-item__wrapper'}>
                <div className={"upload-item__header"}>
                    <h2 className={'upload-item__file-name'}>{name}</h2>
                    <span className={'upload-item__file-percent'}>{Number(progress.toFixed(1))}%</span>
                </div>
                <div className={'upload-item__progress-bar'}>
                    <div className={'upload-item__progressed'} style={{width: `${progress}%`}}/>
                </div>
            </div>
        </div>
    );
};

export default FileUploaderItem;