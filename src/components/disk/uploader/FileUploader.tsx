import React from 'react';
import FileUploaderItem from "./FileUploaderItem";
import '../../../styles/uploader.scss';
import CloseCircle from '../../icons/CloseCircle';
import {useTypedSelector} from "../../../hooks/useTypedSelector";
import {useActions} from "../../../hooks/useActions";

const FileUploader = () => {
    const {showBar, files} = useTypedSelector(state => state.progress);
    const {progressHide} = useActions()
    return (
        <>
            {showBar &&
            <div className={'uploader'}>
                <div className = {'uploader__wrapper'}>
                    <div className = {'uploader__header'}>
                        <h1 className={'uploader__title'}>Загрузка файлов</h1>
                        <div className={"uploader__close"} onClick={()=>progressHide()}> <CloseCircle/></div>
                    </div>
                    <div className={'uploader__content'}>
                        {
                            files.map(f => <FileUploaderItem key={f.id} name={f.name} progress={f.progress}/>)
                        }

                    </div>
                </div>
            </div>}
        </>

    );
};

export default FileUploader;