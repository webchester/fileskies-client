import React from 'react';
import {IFile} from "../../types/files";
import '../../styles/file.scss';
import folderIcon from '../../assets/icons/folder.svg';
import fileIcon from '../../assets/icons/file.svg';
import imageIcon from '../../assets/icons/photologo.svg';
import BxDownloadIcon from "../icons/BxDownloadIcon";
import BxTrashIcon from "../icons/BxTrashIcon";
import {fileRequest} from "../../api/fileRequest";
import size from '../../utils/size';

interface FileProps {
    file: IFile,
    changeCurrentDir: (dir: IFile) => void;
    onDeleteFileHandler: (e: React.MouseEvent<HTMLButtonElement>, id: string) => Promise<void>;
    switcher: string;
}

const File:React.FC<FileProps> = ({file, changeCurrentDir, onDeleteFileHandler, switcher}) => {
    const currentFileIcon = (type: string): string => {
        switch (type){
            case 'dir':
                return folderIcon;
            case 'jpg':
                return imageIcon;
            case 'png':
                return imageIcon;
            case 'jpeg':
                return imageIcon;
            default:
                return fileIcon;
        }
    }
    const downloadFileHandler = async (file: IFile): Promise<void> => {
       await fileRequest.downloadFile(file);
    }
    if(switcher === 'grid'){
     return <div className={'file-grid-item'}  onClick={() => changeCurrentDir(file)}>
         <div className={'file-grid-item__wrapper'}>
             <img className={'file-grid-item__icon'}
                  src={currentFileIcon(file.type)} alt={'file_icon'}/>
            <div className={'file-grid-item__title'}>
                {file.name}
            </div>
             <div className={'file-grid-item__buttons'}>
                 {file.type !=='dir' &&
                 <button className={'btn btn--outlined'} onClick={() => downloadFileHandler(file)}><BxDownloadIcon/></button>}
                 <button className={'btn btn--outlined'} onClick={(e) => onDeleteFileHandler(e, file._id)}><BxTrashIcon/></button>
             </div>
         </div>
     </div>
    }else{
        return (
            <div className= {"file"} onClick={() => changeCurrentDir(file)}>
                <img
                    className={'file__column file__icon file__column--icon'}
                    src={currentFileIcon(file.type)} alt={'file_icon'}/>
                <div className={"file__column file__column--name"} title={file.name}>
                    {file.name}
                </div>
                <div className={"file__column file__column--date"}>
                    {file.date.slice(0,10)}
                </div>
                <div className={"file__column file__column--size"}>
                    {file.type !== "dir" && size(Number(file.size))}
                    <div className={"file__column file__column--edit"}>
                        {file.type !=='dir' &&
                        <button className={'btn btn--outlined'} title={'Скачать'} onClick={() => downloadFileHandler(file)}><BxDownloadIcon/></button>}
                        <button className={'btn btn--outlined'} title={'Удалить'} onClick={(e) => onDeleteFileHandler(e, file._id)}><BxTrashIcon/></button>
                    </div>
                </div>

            </div>
        );
    }

};

export default File;