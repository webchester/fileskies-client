import React from 'react';
import '../../styles/fileList.scss';
import File from "./File";
import {IFile} from "../../types/files";

interface FileListProps{
    files: IFile[];
    changeCurrentDir: (dir: IFile) => void;
    onDeleteFileHandler: (e: React.MouseEvent<HTMLButtonElement>, id: string) => Promise<void>;
    switcher: string
}

const FileList:React.FC<FileListProps> = ({files, changeCurrentDir, onDeleteFileHandler, switcher}) => {


    if (switcher === 'grid') {
    return <div className={'file-grid'}>
        <div className={"file-grid__wrapper"}>
                {files && files.map( file => <File
                    key={file._id}
                    switcher={switcher}
                    changeCurrentDir={changeCurrentDir}
                    file={file}
                    onDeleteFileHandler={onDeleteFileHandler}/>)}
        </div>
    </div>
    }else{
        return (
            <div className={'file-list'}>
                <div className={"file-list__wrapper"}>
                    <div className={"file-list__header"}>
                        <div className={"file-list__column file-list__column--name"}>
                            <h1 className={'file-list__column-name'}>Название</h1>
                        </div>
                        <div className={"file-list__column file-list__column--data"}>
                            <h1 className={'file-list__column-name'}>Дата</h1>
                        </div>
                        <div className={"file-list__column file-list__column--size"}>
                            <h1 className={'file-list__column-name'}>Размер</h1>
                        </div>
                    </div>
                    <div className={"file-list__content"}>
                        {files && files.map( file => <File
                            key={file._id}
                            switcher={switcher}
                            changeCurrentDir={changeCurrentDir}
                            file={file}
                            onDeleteFileHandler={onDeleteFileHandler}/>)}
                    </div>
                </div>
            </div>
        );
    }


};

export default FileList;