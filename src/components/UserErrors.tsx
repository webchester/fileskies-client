import React from "react";
import '../styles/userErrors.scss';
import {useActions} from "../hooks/useActions";
import BxXIcon from './icons/BxXicon';

interface UserErrorsProps { 
    error: string;
}

const UserErrors: React.FC<UserErrorsProps> = ({error}) => {
    const {removeError} = useActions();
    return(
        <div className='user-error'>
            <div className={'user-error__delete'} onClick={ () => removeError() }> <BxXIcon/> </div>
            <div className='user-error__message'>
                {error}
            </div>
        </div>
    ) 
}

export default UserErrors;