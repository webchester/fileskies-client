import React from 'react';

interface BtnIconProps{
    size?: number | null;
    children: React.ReactNode;
}

const BtnIcon:React.FC<BtnIconProps> = (
    {
        size,
        children}) => {
    if(!size) size = 30;
    return (
        <div className={'btnIcon'}  style = {{width: size, height: size }}>
          <div className={'btnIcon__content'}>
              {children}
          </div>
        </div>
    );
};

export default BtnIcon;