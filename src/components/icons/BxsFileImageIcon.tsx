import * as React from "react";

function BxsFileImageIcon(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 24 24"
            fill="currentColor"
            height="1em"
            width="1em"
            {...props}
        >
            <path d="M6 22h12a2 2 0 002-2V8l-6-6H6a2 2 0 00-2 2v16a2 2 0 002 2zm7-18l5 5h-5V4zm-4.5 7a1.5 1.5 0 11-.001 3.001A1.5 1.5 0 018.5 11zm.5 5l1.597 1.363L13 13l4 6H7l2-3z" />
        </svg>
    );
}

export default BxsFileImageIcon;
