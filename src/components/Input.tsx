import React, {FC} from "react";
import '../styles/input.scss';

interface InputProps {
    type: string;
    placeholder: string;
    value: string;
    label?:string;
    changeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: FC<InputProps> = ({type, value, placeholder, changeHandler, label}) => {
    return (
        <div className={'form-control'}>
            {label && <label className={'form-control__label'}>{label}</label>}
               <input className={'form-control__input'} value={value} type={type} placeholder={placeholder} onChange={(changeHandler)}/>
        </div>
    );
};

export default Input;