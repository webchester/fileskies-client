import React, {useState} from 'react';
import '../styles/navbar.scss';
import {eRoutes} from '../utils/routes';
import {NavLink} from "react-router-dom";
import Logo from "./Logo";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useActions} from "../hooks/useActions";
import  BxLogInCircleIcon from '../components/icons/BxLogInCircleIcon';
import BtnIcon from "./icons/BtnIcon";
import BxSearchAlt2Icon from "./icons/BxSearchAlt2Icon";
import '../styles/searcbox.scss';

const NavBar:React.FC = () => {
    const [search, setSearch] = useState<string>('');
    const {user, isAuth} = useTypedSelector(state => state.user);
    const {currentDir} = useTypedSelector(state => state.file);
    const [searchTimeout, setSearchTimeout] = useState<any>(false);
    const {logoutUser, searchFiles, setFiles} = useActions();

    const searchHandler = async (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
        if(searchTimeout !== false){clearTimeout(searchTimeout)}
        if(e.target.value !== ''){
            setSearchTimeout( setTimeout(
                async (value) => await searchFiles(value),
                500, e.target.value))
        }else{
            await setFiles(String(currentDir),"type");
        }
    }

    return (
        <div className={'navbar'}>
            <div className={'navbar__wrapper'}>
                <div className={'navbar__left'}>
                  <NavLink to={eRoutes.HOME_ROUTE}><Logo/></NavLink>
                </div>
                <div className={'navbar__middle'}>

                    {isAuth &&
                    <div className={'search-box'}>
                        <input className={'search-box__input'} value={search} onChange={(e) => searchHandler(e)} type={"text"} placeholder={'поиск на сайте'}/>
                        <span className={'search-box__icon'}><BxSearchAlt2Icon/></span>
                    </div>
                    }

                </div>
                <div className={'navbar__right'}>
                    {!isAuth ?
                    <>
                        <NavLink className={'button navbar__button'} to={eRoutes.LOGIN_ROUTE}>
                            Вход
                        </NavLink>
                        {/*<NavLink className={'button navbar__button'} to={eRoutes.REGISTER_ROUTE}>*/}
                        {/*    Регистрация*/}
                        {/*</NavLink>*/}
                    </>
                    :
                    <div className={'user-header'}>
                        <img className={'user-header__avatar'} src={user?.avatar} alt={'user_avatar'}/>
                        <span className={'user-header__name'}>{user?.email}</span>
                        <span className={'user-header__link'} onClick={logoutUser}>
                            <BtnIcon>
                                <BxLogInCircleIcon/>
                            </BtnIcon>
                        </span>
                    </div>
                    }

                </div>
            </div>
        </div>
    );
};

export default NavBar;