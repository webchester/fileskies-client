import React from 'react';
import logo from "../assets/icons/logo.svg";
import '../styles/logo.scss';
import logoWhite from "../assets/icons/logowhite.svg";

interface LogoProps{
    color?: string
}

const Logo:React.FC<LogoProps> = ({color}) => {
    return (
        <div className={'logo'}>
            <img className={'logo__icon'} src={color==='white' ? logoWhite :  logo} alt={'logo'}/>
            <div className={'logo__text'}>File<span className={'logo__text--is-bold'}>Skies</span> </div>
        </div>
    );
};

export default Logo;