import React from 'react';
import '../../styles/footer.scss';
import Logo from "../Logo";
import Errors from "./Errors";
import {NavLink} from "react-router-dom";
import {useTypedSelector} from "../../hooks/useTypedSelector";
import reactSvg from '../../assets/icons/React-icon.svg';
import reduxSvg from '../../assets/icons/redux.svg';
import nodeSvg from '../../assets/icons/nodejs.svg';
import saasSvg from '../../assets/icons/sass.svg';
import typeSvg from '../../assets/icons/typescript.svg';
import mongoSvg from '../../assets/icons/mongodb.svg';
import reduxSagaSvg from '../../assets/icons/redux-saga.svg';
import jvtSvg from '../../assets/icons/jwt-3.svg'
import {eRoutes} from "../../utils/routes";

const Footer = () => {
    const errorsFile = useTypedSelector(state => state.file.errors);
    return (
        <footer className={'footer'}>
            <div className={'footer__wrapper'}>
                <div className={'footer__left-side'}>
                    <Logo />
                    <span className={'footer__about'}>
                        Облачное хранилище, тестовый сервис,<br/>
                        <b>FRONTEND</b>: Typescript, reactjs redux, redux-thunk, sass<br/>
                        <b>BACKEND</b>: Nodejs , express, Mongoose.
                    </span>
                </div>
                <div className={'footer__middle'}>
                    <ul className={'footer__menu'}>
                        <li className={'footer__menu-item'}>
                            <NavLink to={eRoutes.HOME_ROUTE} className={'footer__menu-link'}>
                                Файлы
                            </NavLink>
                        </li>
                        <li className={'footer__menu-item'}>
                            <NavLink to={eRoutes.LOGIN_ROUTE} className={'footer__menu-link'}>
                                Вход
                            </NavLink>
                        </li>
                        <li className={'footer__menu-item'}>
                        <NavLink to={eRoutes.REGISTER_ROUTE} className={'footer__menu-link'}>
                            Регистрация
                        </NavLink>
                    </li>

                    </ul>
                </div>
                <div className={'footer__right'}>
                  <img className="footer__logo-icon" src={reactSvg} alt={'react'}/>
                  <img className="footer__logo-icon" src={reduxSvg} alt={'redux'}/>
                  <img className="footer__logo-icon" src={nodeSvg} alt={'nodejs'}/>
                  <img className="footer__logo-icon" src={saasSvg} alt={'saas'}/>
                  <img className="footer__logo-icon" src={typeSvg} alt={'type Script'}/>
                  <img className="footer__logo-icon" src={mongoSvg} alt={'mongo db'}/>
                    <img className="footer__logo-icon" src={reduxSagaSvg} alt={'redux saga'}/>
                    <img className="footer__logo-icon" src={jvtSvg} alt={'jwt'}/>
                </div>
            </div>
            <div className={'footer__subInfo'}>
             <a
                 className={'footer__submenu-link'}
                 href={'https://gitlab.com/webchester'}
                 rel="noreferrer"
                 title={'Gitlab page'}
                 target={'_blank'}>
                 🤦  Artion Greenbug
             </a>
                <small>🎉 {new Date().getFullYear()}</small>
            </div>
            <div className={"errors-block"}>
                <div className={"errors-block__wrapper"}>
                    {errorsFile && <Errors error={errorsFile}/>}
                    
                </div>

            </div>

        </footer>
    );
};

export default Footer;