import React from 'react';
import logoSvg from '../../assets/icons/logo.svg';
import '../../styles/loader.scss';

const Loader = () => {
    return (
        <div className={'loader'}>
            <div className={"loader__content"}>
                <img className={'loader__logo'} src={logoSvg} alt={'loading'}/>
                <p className={'loader__text'}>загружаю</p>
            </div>
        </div>
    );
};

export default Loader;