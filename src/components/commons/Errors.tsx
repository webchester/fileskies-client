import React from 'react';
import '../../styles/error.scss';
import {useActions} from "../../hooks/useActions";

interface ErrorsProps {
    error: string;
}
const Errors:React.FC<ErrorsProps> = ({error}) => {
    const {deleteError} = useActions()


    return (
       <div className={'error'}>
           <div className={'error__wrapper'}>
                   <div className={'error__header'}>
                       <h1 className={'error__title'}>УПС, ошибка!</h1>
                       <button onClick={ ()=> deleteError()} className={'error__close'} >x</button>
                   </div>
                <div className={'error__content'}>
                    {error}
                </div>


               </div>
       </div>
    )
};

export default Errors;