import React from 'react';
import emptySvg from '../../assets/icons/empty.svg';
import '../../styles/empty.scss';

const Empty = () => {
    return (
        <div className = "empty">
            <div className = "empty__wrapper">
                <img className={'empty__image'} src={emptySvg} alt={'empty'}/>
                <h1 className={'empty__text'}>Ничего не найдено</h1>
                <h1 className={'empty__text'}>Перетащите файлы сюда или нажмите кнопку добавить</h1>
            </div>
        </div>
    );
};

export default Empty;